
import './App.css';
import React from 'react';
import CharacterPages from './pages/charactersPages';
import EpisodePages from './pages/episodesPages';
import LocationPages from './pages/locationsPages'
import { BrowserRouter, Routes, Route, Link} from 'react-router-dom';

export const DataContext = React.createContext(null)
function App() {
  return (
    
    <BrowserRouter>
      <div className="App">
        <div className="App-header">
        
        <div className ="c-nav">
        <Link className="c-navb" to="/">Characters</Link>
        <Link className="c-navb" to="/locations">Locations</Link>
        <Link  className="c-navb" to="/episode">Episode</Link>
</div>
        <Routes>
          <Route path="/">
            <Route index element={ <CharacterPages></CharacterPages> }/>
            <Route path="/locations" element={ <LocationPages></LocationPages> }/>
           
              <Route path="/episode" element={ <EpisodePages></EpisodePages> }/>
             
          </Route>
          
          
          
          {/* <Route path="/users/abel" element={ <UsersAbelPage></UsersAbelPage> }/> */}

          {/* <Route path="/contact" element={ <ContactPage></ContactPage> }/>
          <Route path="/" element={ <HomePage></HomePage> }/> */}
        </Routes>
        </div>
      </div>
    </BrowserRouter>
    

  );
}

export default App;
