import React, { useEffect, useState} from 'react';
import { getEpisodes } from "../api/apiEpisode";

const EpisodePages = () => {

    const [ episodes, setEpisodes]= useState([])

    useEffect (()=>{
        getEpisodesFromApi()
    },[])

    const getEpisodesFromApi = async()=>{
        try {
            const EpisodesFromApi = await getEpisodes()
            setEpisodes(EpisodesFromApi.results)
           
        }catch(error){
            console.log(error)
        }
    }
    return(
        <>
        {episodes.map(item => {
                return(
                  <div  key={JSON.stringify(item)}>
                    <h2>{item.name}</h2>
                    
                    </div>
                )
        })}
        
        </>
    )
}
export default EpisodePages