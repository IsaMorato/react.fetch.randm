import React, { useEffect, useState} from 'react';
import { getLocations } from "../api/apiLocations";

const LocationPages = () => {

    const [ locations, setLocations]= useState([])

    useEffect (()=>{
        getLocationsFromApi()
    },[])

    const getLocationsFromApi = async()=>{
        try {
            const LocationsFromApi = await getLocations()
            setLocations(LocationsFromApi.results)
           
        }catch(error){
            console.log(error)
        }
    }
    return(
        <>
        {locations.map(item => {
                return(
                  <div  key={JSON.stringify(item)}>
                    <h2>{item.name}</h2>
                    <p>{item.species}</p>
                    <p>{item.status}</p>
                    </div>
                )
        })}
        
        </>
    )
}
export default LocationPages