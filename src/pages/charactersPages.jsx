import React, { useEffect, useState} from 'react';
import { getCharacters } from "../api/apiCharacters";
import './charactersPage.css'

const CharacterPages = () => {

    const [ characters, setCharacters]= useState([])

    useEffect (()=>{
        getCharactersFromApi()
    },[])

    const getCharactersFromApi = async()=>{
        try {
            const CharactersFromApi = await getCharacters()
            setCharacters(CharactersFromApi.results)
           
        }catch(error){
            console.log(error)
        }
    }
    return(
        <>
         
        {characters.map(item => {
                return(
                  
                <div className = "c-card">
                  <div className="c-char" key={JSON.stringify(item)}>
                    <h2>{item.name}</h2>
                    <img src= {item.image} alt ={item.name}></img> 
                    <p>{item.species}</p>
                    <p>{item.status}</p>
                    
                    </div>
                    </div>
                    

                )
        })}
        
        </>
    )
}
export default CharacterPages