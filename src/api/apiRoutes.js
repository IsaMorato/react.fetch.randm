const BASE_URL =" https://rickandmortyapi.com/api"
const GET_CHARACTERS= `${BASE_URL}/character`
const GET_LOCATIONS= `${BASE_URL}/location`
const GET_EPISODES= `${BASE_URL}/episode`


export {
    BASE_URL, 
    GET_CHARACTERS,
    GET_LOCATIONS,
    GET_EPISODES
}